
<h1 align="center"> Written Communication</h1>


<h1 align="center">How does a browser render HTML, CSS, JS to DOM? What is the mechanism behind it?</h1>

> Hello all, everyone are familiar with web browsers. The purpose of writing this blog is to explain how the browser renders and what are the steps that our browser converts `HTML`, `CSS`, `JavaScript` codes into a working website which we can use as interface. By knowing the process you will get an idea how to improve the web applications regarding the speed and performance that decides the impression on a website.

**let's get into it.**

## INTRODUCTION:


Generally, we know that web browser is a small piece of software that loads files from a remote server or a local disk and displays them to us as a user interface. And there is a core software component named **Browser Engine**, that selects what to display based on the type of the file the browser received.

For every major browser it has it's own browser engine such as:

    * Gecko - Firefox 
    * Blink - Chrome and Microsoft Edge
    * Trident - Internet explorer
    * Webkit - Safari
According to my understanding, browser engine is the key software responsible for rendering the recevied files and displaying those to the user interface.

As we know that `HTML`, `CSS` and `JavaScript` work together to develop a website which is displayed to the user, Now we are going to learn what is happening exactly inside the browser to make the website more impressive.

## Transfering the information:
 We all know that the memory is stored in the form of bytes and that bytes of data is transfered to the internet as information.
When we write some `HTML`, `CSS`, `JavaScript`, the browser takes the raw bytes from our hard disk or network.

<img src="https://blog.logrocket.com/wp-content/uploads/2018/09/computer-receiving-data.png" style="width:75%;display: block;
  margin-left: auto;
  margin-right: auto;" >
  
<p> The browser cannot understand the raw data (which is in the form of bytes) but not the actual HTML code and the raw data should be converted into the form the browser understands and  here our learning about rendering starts.</p>


## Document Object Model(DOM):
_**Document Object Model**_ is a language independent programming interface for HTML and XML documents as tree structure where each node is an object representing a part of document. It represent the document as node and object to change the document structure, style and content. **DOM** gives certain changes to the document with the scripting language such as JavaScript with it's object-oriented representation behaviour.



## Converting the Raw Data of HTML to DOM:

<img src="https://developers.google.com/web/fundamentals/performance/critical-rendering-path/images/full-process.png" style="width:90%;display: block;
  margin-left: auto; 
  margin-right: auto;">
<p>From the above diagram we can see the DOM tree how the raw data is conerted into the understanding language for the browser.</p>

1.   The **Bytes** are converted into **Characters** and this conversion is done based on the character encoding of `HTML` file.
2. The **Characters** are converted to **Tokens**, the Characters in the `HTML` file are divided different groups by parsing based on their `<html>` tags. The parsing of `HTML` file according it's tags properties is called Tokenization.
3. The **Tokens** are converted to **Nodes**, we can see Node as a seperate entity DOM tree.
4. And the final step, **Nodes** are converted to **DOM objects**, The DOM objects establish the parent-child relationships, adjacent-sibling relationships etc.

## The DOM has been created!, Is this enough for rendering?
The Nodes that are created during the conversion of Raw Data of HTML to DOM objects may contain some phrases of tags like `link`, which belongs to `main.css` files. As the browser receives the Raw Data of `CSS` files, it requests to fetch the `main.css` style sheets linked. Now the Raw Data of `CSS` files are converted to **CSSOM**.

## CSS Object Model(CSSOM):
Now everyone knows that DOM is a tree structure object model for `HTML`, In the same way `CSS` has ***CSS Object Model*** as a tree structure. **CSSOM** can be used as set of APIs allowing the manuplation of `CSS` from `javaScript`. It allows users to read and modify `CSS` dynamically.

## Converting the Raw Data of CSS to CSSOM:
The process will be similar to that of HTML up to Node formation and then finally Tree structure i.e.,
<p style="text-align:center"> <strong>Bytes => Characters => Tokens => Nodes => Tree Structure</strong></p>

But here the tree structure is **CSSOM** because here we are dealing with `CSS`.
**CSSOM** works in such a way that the styles affecting the element may come from the parent element or have been set on the element themselves. 

## THE RENDER TREE:
The ***Render*** tree is the combination of the ***DOM*** tree and the ***CSSOM*** tree.

<img src="https://developers.google.com/web/fundamentals/performance/critical-rendering-path/images/render-tree-construction.png" style="width:800px;height:500px;display: block;
  margin-left: auto; 
  margin-right: auto;">

The **Render** tree contains all the information on the **DOM** content and **CSSOM** visible content. The tree cannot show the content that is hidden in the **CSSOM** (i.e., `display: none;`).

**The next step in the rendering is LAYOUT!**

## what is LAYOUT?
Layout is finging the exact size and location of each object that is present in the render tree and paints those objects to the screen. Here the browser use some mathematical approach to figure out the exact size and positon of elements.

**By this step the rendering process is completed for HTML and CSS files. But what about `javaScript`?**

## How JavaScript affects Rendering process?
we all know that JavaScript can modify the content and styling of a page or a website. By this we can say JavaScript can add and remove elements in the  **DOM** tree and it can modify the content in the **CSSOM** tree.

When ever the `HTML Parser` encounters a `<script>` tag we can understand that JavaScript comes into the picture the DOM tree construction is paused. The DOM construction is halted until the exection of script.

The location of `<script>` tag is also very important.
* When we write the   `<script>` tag in the   `<body>` tag of the document, the DOM construction is halted until the content in the `<script>` tag is executed.

 * Whereas, when the `<script>` tag is written in the `<head>` tag, the DOM construction is halted without entering into the `<body>` tag. And the JavaScript cannot change the content in the `<body>` tag.

*  Even when extract script from the external sources like `app.js` files, the same conditions are appilcable to the `<script src= "app.js"></script>` tag.

*  Adding `async` to the `<script>` tag helps DOM construction to continue without halting during the execution of JavaScript.  

**But when you try to change the **CSSOM** construction with `<script>` tag, we have to wait till the CSSOM is ready. That means, the JavaScript execution is halted until the CSSOM tree is ready.**
## Critical Rendering Path(CRP):
The whole time taken by the browser to perform all the steps from taking the raw Bytes to converting and painting them to the screen is called ***Critical Rendering Path***.

The **CRP** will decide the optimized website for performance. A well directed CRP helps the browser to load the page as quickly as possible. 

## CONCLUSION:
1. By this blog, everyone can understand what is happening inside the browser to open a website/page containing `HTML`, `CSS` and `JavaScript` files.
2. This could help the developers to get the knowledge about taking the optmized path for loading the web page quickly.

## REFERENCES:
* https://blog.logrocket.com/how-browser-rendering-works-behind-the-scenes-6782b0e8fb10/
* https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction
* https://developer.mozilla.org/en-US/docs/Glossary/CSSOM
* https://developers.google.com/web/fundamentals/performance/critical-rendering-path/images/full-process.png
* https://imgs.developpaper.com/imgs/o_200126140510render-tree-construction.png








        
